#! /usr/bin/env python
#
# TuXtremSplit - A Linux Xtremsplit file tool
#
# Copyright (C) 2011 zedtux <zedtux@zedroot.org>
#
# TuXtremSplit is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# TuXtremSplit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with TuXtremSplit; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
import logging
from tuxtremsplit import TuXtremSplit

# Try to initialize Launchpad Bug Reporter
try:
    import lpbugreporter
except:
    pass

# Start TuXtremSplit
try:
    from tuxtremsplit.application import Application

    try:
        # Change process name from python to tuxtremsplit
        import dl
        libc = dl.open("/lib/libc.so.6")
        libc.call("prctl", 15, Application().settings.executable_name,
                  0, 0, 0)
    except:
        pass

    import optparse

    if __name__ == "__main__":
        parser = optparse.OptionParser(
            prog=Application().settings.executable_name,
            version=Application().settings.version,
            usage="usage: %prog [options] source_path [destination_path]",
            description=Application().settings.description
        )
        parser.add_option(
            "-d", "--debug", action="store_true",
            help="Enable debuging"
        )
        parser.add_option(
            "-p", "--parts", action="store",
            help="Define the number of parts to split"
        )
        parser.add_option(
            "-s", "--size", action="store",
            help="Define the maximum size of a part to split"
        )
        parser.add_option(
            "-m", "--md5", action="store_true",
            help="Enable/Disable MD5 check"
        )
        parser.add_option(
            "-x", "--sfx", action="store_true",
            help="Generate a SFX Windows executable (auto extract)"
        )
        options, args = parser.parse_args()

        if not options.debug:
            TuXtremSplit().logger.setLevel(logging.INFO)

        # Start application passing arguments otherwise show usage
        Application().start(args, options)

except SystemExit:
    pass
except:
    import traceback
    previous_exception = traceback.format_exc()
    try:
        lpbugreporter.LpBugReporter(application.settings.name)
    except:
        TuXtremSplit().logger.warn("Unable to handle occurred exception "
            "because Launchpad Bug Reporter module is missing.")
        TuXtremSplit().logger.error(previous_exception)
